package techtest.socketsat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class StartupRunner implements CommandLineRunner {

	private static Logger logger = LoggerFactory.getLogger(StartupRunner.class);
	private int socketServerPort = 10003;

	@Override
	public void run(String... args) throws Exception {

		String name;

		try (ServerSocket serverSocket = new ServerSocket(socketServerPort)) {

			Socket socket = serverSocket.accept();
			InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			logger.info("Socket Server waiting for messages.");

			do {
				name = bufferedReader.readLine();
				logger.info("Hello {} from the Socket Server!", name);
			} while (!"".equals(name));
			
		}
	}

}
